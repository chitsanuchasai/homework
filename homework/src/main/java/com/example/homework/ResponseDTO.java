package com.example.homework;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ResponseDTO {
	private Integer shopNo;
	private String shopName;
	private String shopAddress;
	private List<ComputerDTO> comList;
	public Integer getShopNo() {
		return shopNo;
	}
	public void setShopNo(Integer shopNo) {
		this.shopNo = shopNo;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public List<ComputerDTO> getComList() {
		return comList;
	}
	public void setComList(List<ComputerDTO> comList) {
		this.comList = comList;
	}
	
	
}
