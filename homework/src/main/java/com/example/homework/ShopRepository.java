package com.example.homework;

import org.springframework.data.repository.CrudRepository;


public interface ShopRepository extends CrudRepository<ShopEntity, Integer> {

}
