package com.example.homework;


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "shop")
@IdClass(ShopId.class)
public class ShopEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "shop_no", unique = true)
	private Integer shopNo;
	@Id
	@Column(name = "computer_no")
	private Integer computerNo;
	@ManyToOne(cascade = CascadeType.ALL, targetEntity = ComputerEntity.class)
	@JoinColumn(name = "computer_no", insertable = false, updatable = false)
	private ComputerEntity computer;
	@Column(name = "shop_name")
	private String shopName;
	@Column(name = "shop_address")
	private String shopAddress;
	
	
	
	public ShopEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ShopEntity(Integer shopNo, String shopName, String shopAddress,
			ComputerEntity computer) {
		super();
		this.shopNo = shopNo;
		this.shopName = shopName;
		this.shopAddress = shopAddress;
		this.computerNo = computer.getComputerNo();
		this.computer = computer;
	}
	public Integer getShopNo() {
		return shopNo;
	}
	public void setShopNo(Integer shopNo) {
		this.shopNo = shopNo;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public Integer getComputerNo() {
		return computerNo;
	}
	public void setComputerNo(Integer computerNo) {
		this.computerNo = computerNo;
	}
	public ComputerEntity getComputer() {
		return computer;
	}
	public void setComputer(ComputerEntity computer) {
		this.computer = computer;
	}
	
	
	
	
	
}
