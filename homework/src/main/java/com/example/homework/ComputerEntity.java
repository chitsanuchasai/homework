package com.example.homework;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "computer")
public class ComputerEntity {
	@Id
	@Column(name = "computer_no", unique = true)
	private Integer computerNo;
	@Column(name = "computer_name")
	private String computerName;
	@Column(name = "brand")
	private String brand;
	@Column(name = "space")
	private String space;
	
	
	public ComputerEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ComputerEntity(Integer computerNo, String computerName, String brand, String space) {
		super();
		this.computerNo = computerNo;
		this.computerName = computerName;
		this.brand = brand;
		this.space = space;
	}
	
	public Integer getComputerNo() {
		return computerNo;
	}
	public void setComputerNo(Integer computerNo) {
		this.computerNo = computerNo;
	}
	public String getComputerName() {
		return computerName;
	}
	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getSpace() {
		return space;
	}
	public void setSpace(String space) {
		this.space = space;
	}
	

}
