package com.example.homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller // This means that this class is a Controller
@RequestMapping(path = "/Shop")
public class ShopController {
	@Autowired
	private ShopRepository shop;
	@Autowired
	private ComputerRepostity com;

	
	@RequestMapping("/")
	public @ResponseBody Iterable<ShopEntity> getAllShop() {
		return shop.findAll();
	}

	  
	  @PostMapping(path = "/addComputer", produces = MediaType.APPLICATION_JSON_VALUE)
		@ResponseBody ResponseEntity<ResponseDTO> addComputer( @RequestBody RequestDTO req) {
		  ComputerEntity com = new ComputerEntity(req.getComputerno(), req.getComputername(), req.getComputerbrand(), req.getComputerspace());
		  ShopEntity shope = new ShopEntity(req.getShopno(), req.getShopname(), req.getShopaddress(), com);
		  shop.save(shope);
		  List<ShopEntity> shopList = Arrays.asList(shope);
		  
		  ResponseDTO res = new ResponseDTO();
		  res.setShopNo(req.getShopno());
		  res.setShopName(req.getShopname());
		  res.setShopAddress(req.getShopaddress());
		  List<ComputerDTO> comList = new ArrayList<>();
		  for(ShopEntity s : shopList) {
			  ComputerDTO c = new ComputerDTO();
			  c.setComputerNo(s.getComputer().getComputerNo());
			  c.setComputerName(s.getComputer().getComputerName());
			  c.setComputerBrand(s.getComputer().getBrand());
			  c.setComputerSpace(s.getComputer().getSpace());
			  comList.add(c);
		  }
		  res.setComList(comList);
		  return new ResponseEntity<ResponseDTO>(res, HttpStatus.OK);
		}
	  

}
