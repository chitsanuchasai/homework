package com.example.homework;

import java.io.Serializable;

public class ShopId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer shopNo;
	private Integer computerNo;
	
	public ShopId() {
		
	}
	public ShopId(Integer shopNo, Integer computerNo) {
		super();
		this.shopNo = shopNo;
		this.computerNo = computerNo;
	}
	public Integer getShopNo() {
		return shopNo;
	}
	public void setShopNo(Integer shopNo) {
		this.shopNo = shopNo;
	}
	public Integer getComputerNo() {
		return computerNo;
	}
	public void setComputerNo(Integer computerNo) {
		this.computerNo = computerNo;
	}
	
	
	

}
