package com.example.homework;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ComputerDTO {
	private Integer computerNo;
	private String computerName;
	private String computerBrand;
	private String computerSpace;
	public Integer getComputerNo() {
		return computerNo;
	}
	public void setComputerNo(Integer computerNo) {
		this.computerNo = computerNo;
	}
	public String getComputerName() {
		return computerName;
	}
	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}
	public String getComputerBrand() {
		return computerBrand;
	}
	public void setComputerBrand(String computerBrand) {
		this.computerBrand = computerBrand;
	}
	public String getComputerSpace() {
		return computerSpace;
	}
	public void setComputerSpace(String computerSpace) {
		this.computerSpace = computerSpace;
	}
	
	

}
