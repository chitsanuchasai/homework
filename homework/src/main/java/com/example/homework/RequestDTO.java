package com.example.homework;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class RequestDTO {
	private Integer shopno;
	private Integer computerno;
	private String shopname;
	private String shopaddress;
	private String computername;
	private String computerbrand;
	private String computerspace;
	public Integer getShopno() {
		return shopno;
	}
	public void setShopno(Integer shopno) {
		this.shopno = shopno;
	}
	public Integer getComputerno() {
		return computerno;
	}
	public void setComputerno(Integer computerno) {
		this.computerno = computerno;
	}
	public String getShopname() {
		return shopname;
	}
	public void setShopname(String shopname) {
		this.shopname = shopname;
	}
	public String getShopaddress() {
		return shopaddress;
	}
	public void setShopaddress(String shopaddress) {
		this.shopaddress = shopaddress;
	}
	public String getComputername() {
		return computername;
	}
	public void setComputername(String computername) {
		this.computername = computername;
	}
	public String getComputerbrand() {
		return computerbrand;
	}
	public void setComputerbrand(String computerbrand) {
		this.computerbrand = computerbrand;
	}
	public String getComputerspace() {
		return computerspace;
	}
	public void setComputerspace(String computerspace) {
		this.computerspace = computerspace;
	}
	
	
}
